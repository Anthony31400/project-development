# Process TerraNIS

### Les labels

Un standard de labels est définis au niveau du groupe Terranis => plus au niveau.
Ces labels pourront être récupérés par tous les projets se situant à l'intérieur.
Cela n'empêche pas chaque projet d'ajouter ses propres labels si souhaités (labels de scope éventuellement)

Avantage: Homogénéité des labels pour l'ensemble des projets

### Les boards

Les boards peuvent être créés au niveau des groupes et des projets.
Dans le programme Gitlab souscrit, on peut créer un seul board pour les groupes et plusieurs pour les projets.
Il faut conserver le board développement pour les groupes et les trois boards (développement, type et backlog) pour les projets

TODO:

Un script peut être développé pour générer les boards automatiquement en se basant sur les labels du groupe TerraNIS

### Les templates

Chaque projet doit comporter un dossier .gitlab avec un sous-dossier issue_templates contenant les templates.md
Ce dossier doit pouvoir être récupérer sur ce projet

### Architecture des groupes et des projets

Tous les projets prenant part à un même sujet de Terranis doivent être rassemblés sous un groupe cf exemple

```mermaid
erDiagram
Terranis ||--|{ agri: ""
Terranis ||--|{ envi: ""
Terranis ||--|{ is_ia: ""
Terranis ||--|{ viti: ""
Terranis ||--|{ envi: ""
Terranis ||--|{ is_ia: ""
viti ||--|{ Oenoview: ""
Oenoview {
    string Oenoview_app
    string Oenoview_web
    string Oenoview_api
    string etc
}
agri ||--|{ Pixagri: ""
Pixagri {
    string Pixagri-web
    string Pixagri-app
    string Pixagri-api
    string etc
}

```

### Les issues

Les issues peuvent être visualisées depuis un groupe supérieur.
Ainsi depuis le board terranis on peut visualiser l'ensemble des issues existant sur le groupe général.
Sachant qu'il est possible de filtrer les issues par affectation et autres, il est donc facile d'évaluer la charge de travail pour une personne ou un projet.
