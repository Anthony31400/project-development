# Labels

## Definition

les labels sont des catégories applicables aux issues. Ils permettent de filtrer les issues, trier les tableaux et visualiser l'avancement d'un projet.
Plusieurs labels peuvent être affectés à une issue.
Les labels peuvent avoir plusieurs signifcations.

## Catégorie de labels

## Les labels d'état

Les label d'état représentent le cycle de vie d'une issue.

<!-- TO DO: Ajouter l'image des labels comme ils sont représenté sur gitlab  -->
<ul>
<li class="item"><span class="todo">TO DO</span> : signifie que l'issue doit être traitée</li>
<li class="item"><span class="inProgress">IN PROGRESS</span> : signifie que l'issue est en cours de traitement</li>
<li class="item"><span class="toValidate">TO VALIDATE</span> : signifie que l'issue a été réalisé et qu'elle est en attente de validation par un validateur</li>
<li class="item"><span class="completed">COMPLETED</span> : signifie que l'issue a été réalisé et validé.</li>
</ul>

Ces labels sont automatiquement affectés en fonction de leur position dans le [board](../boards/Board.md)

## Les labels de priorité

Les labels de priorité représentent l'importance de traitement d'une issue

<ul>
<li class="item"><span class="low">Low</span> : Faible priorité</li>
<li class="item"><span class="medium">Medium</span> : Priorité moyenne</li>
<li class="item"><span class="high">High</span> : Haute priorité</li>
<li class="item"><span class="critical">Critical</span> : Priorité critique</li>
</ul>

Le choix des labels de priorité dépendent du type de l'issue.

- Pour définir un label de priorité pour un bug cliquez [ici](../bugs/BugReporting.md)
- Pour définir un label de priorité pour une feature cliquez [ici](../features/FeatureReporting.md)

## Les labels de types

Les labels de type définissent le type de l'issue - <span class="bug">[Bug](../bugs/BugReporting.md)</span> - <span class="feature">[Feature](../features//FeatureReporting.md)</span> - <span class="optimisation">[Change](../changes/ChangesReporting.md)</span>

## Les labels de scopes

Plus aléatoires, les labels de scope sont relatifs au projet. Ils permettent de définir un périmètre à l'issue

- Parcelle
- Administration
  etc...

Ces labels facultatifs doivent être affectés manuellement.

#### Voir également

- [issues](../issues/Issue.md)
- [boards](../boards/Board.md)
