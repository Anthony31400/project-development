<!--Explain board -->

# Board

Un board est un tableau d'[issues](../issues/Issue.md) comportant plusieurs colonnes relatives à [label](../labels/Labels.md).
Dès lors qu'une issue est placée dans une des colonnes, le label de cette dernière lui est automatiquement affecté.
Les issues peuvent être déplacées d'une colonne à l'autre

## Board Developpement

Board principal d'un projet => il représente l'état d'avancement des issues.

Il est composé de 4 colonnes :

<ul>
<li class="item"><span class="todo">TO DO</span></li>
<li class="item"><span class="inProgress">IN PROGRESS</span></li>
<li class="item"><span class="toValidate">TO VALIDATE</span></li>
<li class="item"><span class="completed">COMPLETED</span></li>
</ul>

## Board Backlog

Board secondaire d'un projet => il permet de prioriser les issues.
Il est composé de 4 colonnes:

<ul>
<li class="item"><span class="low">Low</span></li>
<li class="item"><span class="medium">Medium</span></li>
<li class="item"><span class="high">High</span></li>
<li class="item"><span class="critical">Critical</span></li>
</ul>

## Board Types

Board secondaire d'un projet qui permet de classifier les issues
Il est composé de 3 colonnes:

<ul>
<li class="item"><span class="bug">Bug</span></li>
<li class="item"><span class="feature">Feature</span></li>
<li class="item"><span class="optimisation">Change</span></li>
</ul>

#### Voir également

- [issues](../issues/Issue.md)
- [labels](../labels/Labels.md)
