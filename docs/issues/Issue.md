# Issues

Une issue permet la déclaration d'un besoin concernant un projet.
Elle doit être précise et concerner une seule problématique à la fois. Elle peut être revu, assigné, labellisé.
L'avantage d'une issue est qu'elle possède un historique d'évolution et de discussion.

## Déclaration d'un issue

### Utilisation du template

Avant d'ouvrir une issue il est important de se demander quel est type d'issue qu'on souhaite ouvrir. Il existe 3 types d'issue :

- [Feature](../features/FeatureReporting.md): Ajout d'un fonctionnalité
- [Bug](../bugs/BugReporting.md): Fonctionnalité existante qui n'a pas le comportement souhaité
- [Change](../changes/ChangesReporting.md): Fonctionnalité existante dont on souhaite améliorer le comportement

Pour ces trois cas, il existe des templates prédéfinis qui permettent de faciliter le renseignement du besoin et qui aideront à la priorisation

### Estimation

Une fois l'issue ouverte, elle doit être chiffrée avant d'entrer dans le processus de développement. Deux aspects principaux sont pris en compte:

- Le temps de réalisation: combien de jours de travail
- L'impact sur le système: le nombre de projet impacter par ce besoin

Lors de l'estimation, l'issue peut être redécoupée notamment si elle a un gros impact et/ou impacte plusieurs projets.

### Process de développement

Dès lors que l'issue est rédigé puis estimé, elle peut être planifiée pour être résolue. Elle doit être affectée à la personne qui sera en charge de réaliser le développement et cette dernière se chargera de la faire évoluer dans les boards.

### Testing

Une fois qu'une proposition de résolution est établie, le besoin doit être testé par un validateur (auteur de l'issue par exemple). C'est lui qui est en charge de certifier si la solution répond correctement au besoin ou non.
Une fois que ce dernier aura validé la proposition, l'issue poura être déclaré comme "closed"

## FlowChart

```mermaid
flowchart
    id1(Expression du besoin) --> id2["Redaction de l'issue avec template \n Priorisation (high, medium, low)"]
    id2--> id3{Informations suffisante ?} --> |non| id2
    id3 --> |oui| id4[Estimation d'impact, chiffrage]
    id4 --> id5{Planification pour le sprint ?}
    id5 --> |non| id6(Status reste OPEN)
    id5 --> |oui| id7[Affectation de l'issue et passage en TO DO]
    id7 --> id8[En cours de réalisation passage en in progress]
    id8 --> id9[Finalisation, passage en To Validate \nDéploiement dev pour testeur \n affectation issue au testeur]
    id9 --> id10{L'issue répond au besoin ?}
    id10 --> |non| id8
    id10 --> |oui| id11(Affectation en completed)
```

#### Voir également

- [labels](../labels/Labels.md)
- [boards](../boards/Board.md)
- [Feature](../features/FeatureReporting.md)
- [Bug](../bugs/BugReporting.md)
- [Change](../changes/ChangesReporting.md)
