# Feature reporting

## Definition d'une feature

Une feature est l'ajout d'une fonctionnalité dans une application.

## Utiliser le template

### Step 1 (Description)

Décrire avec le plus de précision possible le besoin souhaité.

### Step 2 (Use case)

Donner des cas d'usage de la nouvelle fonctionnalité de façon à mettre en images la fonctionnalité.
Cela permet également au développeur de définir sous quel angle il pourra aborder cette fonctionnalité

### Step 3 (Who will benefit?)

Définissez dans quel cadre cette fonctionnalité doit être développée et pour qui. Cela permet surtout de prioriser la feature.

### Step 4 (Example)

Si des fonctionnalités existe ailleurs, vous pouvez fournir des screenshots ou autres

### Step 5 ( Links / references)

S'il existe de la documentation d'api, des designs front ou quoi que ce soit d'autres utiles pour le développement de la feature, vous pouvez les ajouter ici

### Step 6 (Validation requirements)

Définissez les critères d'acceptance de la feature nécessaire pour clore l'issue

## Prioriser la feature

Pour traiter efficacement les features, ils ont besoin d'être priorisé.
Une fois le template de bug report rempli, vous devez lui affecter un des 4 labels suivants:

### High

Feature indispensable => sans cette fonctionnalité, la valeur du projet est grandement affectée voir nulle

### Medium

La feature n'est pas indispensable mais elle apporte une valeur ajoutée au projet importante

### Low

La feature relève du détail et a un impact minime sur l'application
