# Bug reporting

## Définition d'un bug

Un bug est un comportement inattendu sur une fonctionnalité **déjà existante**. Cela peut se manifester par un plantage total de l'application, une mauvaise réponse ou un mauvais affichage.

## Utiliser le template

### Step 1 (Title)

Définir un titre court et le plus explicite possible pour cibler l'origine du bug.

### Step 2 (Rapide informations)

Fournir des informations permettant de contextualiser l'apparition du bug
Des informations comme le navigateur utilisé ou le device peuvent être très utiles.

### Step 3 (What Happened)

Détailler ce qu'il s'est produit lorsque vous avez tenté de réaliser cette action
Ne pas hésiter à fournir le message d'erreur affiché si présent.

### Step 4 (Expected Result)

Expliquez ce a quoi vous vous attendiez

### Step 5 (Steps to reproduce)

Détaillez précisément les manipulations que vous avez faites pour être face au bug. La répétabilité du bug est nécessaire pour le résoudre.

### Step 6 (Screenshots)

Si vous avez des screenshots vous pouvez les fournir

## Prioriser le bug

Pour traiter efficacement les bugs, ils ont besoins d'être priorisé.
Une fois le template de bug report rempli, vous devez lui affecter un des 4 labels suivants:

### Critical

Le plus haut niveau de priorisation, critical rend l'application et ou tout un process inutilisable

### High

L'expérience utilisateur est fortement affectée mais il est possible de réaliser le process de bout en bout

### Medium

L'expérience utilisateur est affectée mais n'a que très peu d'effet sur le process

### Low

L'expérience utilisateur est faiblement affectée et le process est totalement réalisable
