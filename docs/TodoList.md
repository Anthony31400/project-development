# To do list

### Documentations

- [ ] doc & process sprint & milestone
- [ ] doc & process testing
- [ ] doc & process shared files (linter, configs)
- [ ] doc & process inside project convention (naming files, design pattern, documentation etc...)
- [ ] doc & process ci

### Research

- [ ] Milestone

### Scripts

- [ ] create configs with labels
- [ ] script push board with labels
