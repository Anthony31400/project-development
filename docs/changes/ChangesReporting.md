# Changes reporting

## Definition d'un Changes

Un change est une amélioration d'une fonctionnalité existante.
Il peut s'agir d'une amélioration de performance et/ou de résultat

## Utiliser le template

### Step 1 (Area of the system)

Définir quel est le périmètre de ce changement, s'il impacte plusieurs process et/ou plusieurs projets

### Step 2 (How does this currently work?)

Définir comment fonctionne actuellement cette fonctionnalité

### Step 3 (What is the desired way of working?)

Définir comment cette fonctionnalité doit évoluer

### Step 4 (Validation requirements)

Définissez les critères d'acceptance de la feature nécessaire pour clore l'issue
