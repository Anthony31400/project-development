export interface BoardProjectLabel {
  id: number;
  name: string;
  project: {
    id: number;
    name: number;
    name_with_namespace: string;
    path: string;
    path_with_namespace: string;
    http_url_to_repo: string;
    web_url: string;
  };
  milestone: {
    id: number;
    title: string;
  };
  lists: {
    id: number;
    label: {
      name: string;
      color: string;
      description: string | null;
    };
    position: number;
    max_issue_count: number;
    max_issue_weight: number;
    limit_metric: number | null;
  }[];
}
