export interface PutLabelProjectModel {
  /**
   * 	The ID or URL-encoded path of the project owned by the authenticated user
   */
  id: string;
  /**
   * The ID or title of a group’s label.
   */
  label_id: string;
  new_name: string;
  color: string;
}
