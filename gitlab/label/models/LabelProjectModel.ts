export interface LabelProjectModel {
  id: number;
  name: string;
  /**
   * HEX
   */
  color: string;
  text_color: string;
  description: string;
  description_html: string;
  open_issues_count: number;
  closed_issues_count: number;
  suscribed: boolean;
  priority: number;
  is_project_label: boolean;
}
