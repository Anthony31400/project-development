export interface PostLabelProject {
  /**
   * 	The ID or URL-encoded path of the project owned by the authenticated user
   */
  id: string;
  name: string;
  color: string;
  description?: string;
  priority?: number;
}
