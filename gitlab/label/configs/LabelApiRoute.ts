import { getProjectIdRoute } from "../../project/configs/ProjectApiRoute";

export const LABEL_ENDPOINT = "/labels";
export const ID_LABEL_PARAM = ":labelId";
export const LABEL_ROUTE = `${LABEL_ENDPOINT}/${ID_LABEL_PARAM}`;

export const getLabelRoute = (projectId: string) => {
  const projectRoute = getProjectIdRoute(projectId);
  return `${projectRoute}/${LABEL_ENDPOINT}`;
};

export const getSingleLabelRoute = (projectId: string, labelId: string) => {
  const projectRoute = getProjectIdRoute(projectId);
  return `${projectRoute}${LABEL_ROUTE.replace(ID_LABEL_PARAM, labelId)}`;
};
