import { ROOT_API_URL } from "../../configs/GitlabApiRoute";

export const PROJECT_ENDPOINT = "/project";
export const ID_PROJECT_PARAM = ":projectId";
export const PROJECT_ROUTE = `${PROJECT_ENDPOINT}/${ID_PROJECT_PARAM}`;

export const getProjectIdRoute = (id: string) => {
  return `${ROOT_API_URL}${PROJECT_ROUTE.replace(ID_PROJECT_PARAM, id)}`;
};
