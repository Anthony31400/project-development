<!-- Write a short title to define the issue  -->

## Description

<!-- Describe the feature wanted-->

### Use case

<!-- fill some use cases-->

## Who will benefit?

<!-- Will this fix a problem that only one user has, or will it benefit a lot of people !-->

## Examples

<!-- Are there any examples of this which exist in other software? !-->

## Links / references

<!-- design, api, documentations etc... everything usefull to do the feature !-->

## Validation requirements

<!-- Define your validation requirements to close feature -->
